package com.polites.android.example;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Window;

public class StandardImageXML extends Activity {
	com.polites.android.GestureImageView Image;

    @Override
    
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
    	String img=getIntent().getStringExtra("img");
    	InputStream is = null;
	
		  try {
			is = this.getResources().getAssets().open(img);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		  final Bitmap bimage = BitmapFactory.decodeStream(is);

			Image.setImageBitmap(bimage);
		
    	setContentView(R.layout.standard_image);
    }
}