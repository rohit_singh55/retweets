package com.example.TwitterRetweeted;





import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Main extends Fragment {

	Typeface tf,robo;
	int position;
	 PlaceSlidesFragmentAdapter mAdapter;
	    ViewPager mPager;
	  String[] array;
	  public Main() {
		// TODO Auto-generated constructor stub
	}
	   public Main(String[] arr,int pos){
		   array=arr;
		   position=pos;
	   }
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	View rootview=inflater.inflate(R.layout.home, container,false);
	 
	    if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
	        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	    } else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
	       getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    } 
	   Typeface robo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Thin.ttf");
	   
	  //img.setBackgroundResource(R.drawable.zobuwave);
	  
  mAdapter = new PlaceSlidesFragmentAdapter(
          getActivity(),getFragmentManager());

  mPager = (ViewPager) rootview.findViewById(R.id.pager);
  
  mPager.setAdapter(mAdapter);
  mPager.setCurrentItem(position);
  
return rootview;

 
}
	public class PlaceSlidesFragmentAdapter extends FragmentPagerAdapter 
	{
		 Context ctxt=null;





	public PlaceSlidesFragmentAdapter(Context ctxt,FragmentManager fm) {
	super(fm);
	this.ctxt=ctxt;
	}

	@Override
	public Fragment getItem(int position) {
		return(new PlaceSlideFragment(array[position]));
	}

	@Override
	public int getCount() {
	return array.length;
	}


	}
}
