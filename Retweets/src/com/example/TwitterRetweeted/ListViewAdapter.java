package com.example.TwitterRetweeted;

import java.util.ArrayList;
import java.util.List;


import com.squareup.picasso.Picasso;







import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import android.sax.StartElementListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import android.widget.TextView;

public class ListViewAdapter extends BaseAdapter {
	String[] array;
	Context mContext;
    LayoutInflater inflater;
    int j=0;
    private List<Tweet> list = null;
    private ArrayList<Tweet> arraylist;
    public ListViewAdapter(Context context, List<Tweet> list) {
    	array=new String[list.size()];
    	 mContext = context;
         this.list = list;
         inflater = LayoutInflater.from(mContext);
    	this.arraylist = new ArrayList<Tweet>();
         this.arraylist.addAll(list);
         for(Tweet l:list){
        	 array[j++]=l.retweeted_status.user.profile_image_url;
         }
	}
    public class ViewHolder {
        TextView title;
       ImageView pic;
    }
    
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Tweet getItem(int position) {
		// TODO Auto-generated method stub
		 return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		Typeface tf,robo;
		final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_rowitem, null);
            // Locate the TextViews in listview_item.xml
            holder.title = (TextView) view.findViewById(R.id.tweet);
            holder.pic=(ImageView) view.findViewById(R.id.list_image);
            view.setTag(holder);
	}else 
		
        holder = (ViewHolder) view.getTag();
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/amplify.ttf");
    	robo = Typeface.createFromAsset(mContext.getAssets(), "fonts/Roboto-Thin.ttf");
        holder.title.setText(list.get(position).getText());
        holder.title.setTypeface(robo);
        Picasso.with(mContext).load(list.get(position).getRetweeted_status().user.profile_image_url).into(holder.pic);
      holder.pic.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent i=new Intent(mContext, ImageDisplay.class);
			i.putExtra("arr", array);
			i.putExtra("pos", position);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mContext.startActivity(i);
		}
	});
	
		
	
		return view;
	}

}

