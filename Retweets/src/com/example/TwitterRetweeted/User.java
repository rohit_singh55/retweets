package com.example.TwitterRetweeted;

import com.google.gson.annotations.SerializedName;

public class User {
	@SerializedName("profile_image_url")
	String profile_image_url;

	public String getProfile_image_url() {
		return profile_image_url;
	}

	public void setProfile_image_url(String profile_image_url) {
		this.profile_image_url = profile_image_url;
	}
}
