package com.example.TwitterRetweeted;

import com.polites.android.GestureImageView;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

public final class PlaceSlideFragment extends Fragment {
    String imageResourceId;

    public PlaceSlideFragment() {
		super();
		// TODO Auto-generated constructor stub
		 imageResourceId = "";
	}

	public PlaceSlideFragment(String i) {
        imageResourceId = i;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        GestureImageView image = new GestureImageView(getActivity());
        
        Picasso.with(getActivity()).load(imageResourceId).into(image);
        LinearLayout layout = new LinearLayout(getActivity());
        
        layout.setGravity(Gravity.CENTER);
        layout.addView(image);

        return layout;
    }
}